const selectUserByIdQuery = require('../../models/users/selectUserByIdQuery');
const selectUserByIdPublicQuery = require('../../models/users/selectUserByIdPublicQuery');

const getUserPublic = async (req, res, next) => {
  try {
    // Obtenemos el path param userId en la propiedad params del obtejo request.
    const { userId } = req.params;

    let user;
    if (userId != req.user.id) {
      user = await selectUserByIdPublicQuery(userId);
    } else {
      user = await selectUserByIdQuery(userId);
    }

    res.send({
      status: 'ok',
      data: {
        user,
      },
    });
  } catch (err) {
    next(err);
  }
};

module.exports = getUserPublic;
