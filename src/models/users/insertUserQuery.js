const getDB = require('../../db/getDB');
const bcrypt = require('bcrypt');

const insertUserQuery = async (
  lastName,
  firstName,
  email,
  password,
  bio = '',
  avatar = ''
) => {
  let connection;
  try {
    connection = await getDB();

    const hashedPass = await bcrypt.hash(password, 10);

    await connection.query(
      `INSERT INTO users (firstName, lastName, email, password, bio, avatar, createdAt) 
             VALUES (?, ?, ?, ?, ?, ?, ?)`,
      [firstName, lastName, email, hashedPass, bio, avatar, new Date()]
    );
  } finally {
    if (connection) connection.release();
  }
};

module.exports = insertUserQuery;
