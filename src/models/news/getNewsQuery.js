const getDB = require('../../db/getDB');

const generateError = require('../../services/generateError');

const getNewsQuery = async (newsId, userId = 0) => {
  let connection;

  try {
    connection = await getDB();

    const [news] = await connection.query(
      `
                SELECT
                    N.id,
                    N.title,
                    N.leadIn,
                    N.body,
                    N.photo,
                    N.categoryId,
                    C.name AS categoryName,
                    U.firstName,
                    U.lastName,
                    N.userId,
                    N.userId = ? AS owner,
                    CAST(SUM(CASE WHEN V.likes=false THEN 1 ELSE 0 END) AS UNSIGNED) AS negativeVotes,
                    CAST(SUM(CASE WHEN V.likes=true THEN 1 ELSE 0 END) AS UNSIGNED) AS positiveVotes,
                    N.createdAt
                FROM news N
                INNER JOIN users U ON U.id = N.userId
                LEFT JOIN votes V ON N.id = V.newsId
                INNER JOIN categories C ON N.categoryId = C.id
                WHERE N.id = ?
                GROUP BY N.id
      `,
      [userId, newsId]
    );

    if (news.length < 1) generateError('Noticia no encontrada', 404);

    return news[0];
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getNewsQuery;
